import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;

/**
 * Command line interface to store the first name, last name, and phone number of a contact
 * @author Joseph Malandruccolo
 *
 */
public class S3CommandLineApp {
	
	
	//===========================================================================
	//	=>	AWS OBJECTS
	//===========================================================================
	static AmazonEC2      ec2;
    static AmazonS3       s3;
    static AmazonSimpleDB sdb;
	
	
    //===========================================================================
  	//	=>	MAIN METHOD
  	//===========================================================================
	public static void main (String[] args) {
		
		
		//	UI
		System.out.println("===========================================");
        System.out.println("=>\tWelcome to the S3 Contact Mgmt System");
        System.out.println("===========================================\n\n");
		
        
        //	Get AWS credentials
		System.out.println("Before starting, please fill in your AWS access credentials in the AwsCredentials.properties file\n\n");
		System.out.println("For your convenience, you can specify credentials now\n");
		try {
			setKeys();
		} catch (IOException e1) {
			System.out.println("Write to file was corrupted, please try again\n");
			System.exit(1);
		}
		try { init(); } 
		catch (Exception e) { 
			System.out.println("Credentials file not found\nPlease create or update a proprerty file at the root of your classpath"); 
			System.exit(1);
		}
		
		
		
		
		
		
		//	prompt the user for input to determine action
		while (true) {
			
			//	UI
			System.out.println("Navigate this AWS command line app by choosing a number\n");
			System.out.println("1 - Create a new bucket\n");
			System.out.println("2 - Show available buckets\n");
			System.out.println("3 - List the contents of a bucket\n");
			System.out.println("4 - Add an object to a bucket\n");
			System.out.println("5 - Delete an object from a bucket\n");
			System.out.println("6 - Edit an existing object\n");
			System.out.println("9 - Exit");
			
			//	get user input
			Scanner s = new Scanner(System.in);
			String rawInput = s.nextLine();
			int parsedInput = 0;
			try { parsedInput = Integer.parseInt(rawInput); } 
			catch (NumberFormatException e) { System.out.println("Please enter a valid number\n"); };
			
			//	handle user input
			switch (parsedInput) {
			case 1:
				createBucket();
				break;
				
			case 2:
				listBuckets();
				break;
				
			case 3:
				listBucketContents();
				break;
				
			case 4:
				try {
					createAndAddObjectToBucket();
				} catch (IOException e) {
					System.out.println("Failed to create and add object to bucket");
				}
				break;
				
			case 5:
				deleteObject();
				break;
				
			case 6:
				try {
					editExistingObject();
				} catch (IOException e) {
					System.out.println("Failed to edit an object");
				}
				break;
				
			case 9:
				System.exit(1);
				break;

			default:
				//	do nothing, continue in while loop
				break;
			}
			
		}//end while
	}//	end main
	
	
	
	//===========================================================================
	//	=>	CONSOLE TRIGGERED METHODS
	//===========================================================================
	
	/**
	 * create a new bucket
	 */
	private static void createBucket() {
		
		//	UI
		System.out.println("Creating a bucket!\n");
		System.out.println("Bucket names must be both unique and DNS compliant\n");
		
		
		
		boolean bSuccess = false;
		String finalBucketName = "";
		while (!bSuccess) {
			try { 
				System.out.println("Please enter a name for your bucket:\n");
				Scanner s = new Scanner(System.in);
				String requestedBucketName = s.nextLine();
				s3.createBucket(requestedBucketName);
				bSuccess = true;
				finalBucketName = requestedBucketName;
			}
			catch (AmazonServiceException e) { System.out.println("Request failed.\nPlease choose a globally unique name that is DNS compliant\n"); }
		}
		
		System.out.println("Successfully created bucket named: " + finalBucketName);
		
	}
	
	
	
	/**
	 * list all buckets
	 */
	private static void listBuckets() {
		
		//	UI
		System.out.println("Listing buckets\n\n");
		
		for (Bucket bucket : s3.listBuckets()) System.out.println(bucket.getName());
		
		//	UI
		System.out.println("---------------------------------\nend of buckets");
		
	}
	
	
	
	/**
	 * show the contents of the bucket
	 */
	private static void listBucketContents() {
		//UI
		System.out.println("Which bucket would you like to examine\n-------------------------");
		listBuckets();
		System.out.println("Enter the name of the bucket you would like to create the object in");
		Scanner s = new Scanner(System.in);
		String bucketName = s.nextLine();
		
		
		ObjectListing objects = s3.listObjects(bucketName); 

		List<S3ObjectSummary> list = objects.getObjectSummaries();
		for(S3ObjectSummary object: list) {
		    S3Object obj = s3.getObject(bucketName, object.getKey());
		    System.out.println(obj.getKey());
		}
	}
	
	
	/**
	 * search for an object in a particular bucket and delete it
	 */
	private static void deleteObject() {
		
		//	UI
		System.out.println("Delete an object in a bucket from the list below\n");
		listBuckets();
		System.out.println("Enter the name of the bucket to see its items");
		
		Scanner s = new Scanner(System.in);
		String bucketName = s.nextLine();
		
		//	UI
		System.out.println("Objects for " + bucketName + "\n________________________");
		
		
		ObjectListing objects = s3.listObjects(bucketName);
		List<S3ObjectSummary> list = objects.getObjectSummaries();
		for(S3ObjectSummary object: list) {
		    S3Object obj = s3.getObject(bucketName, object.getKey());
		    System.out.println(obj.getKey());
		}
		
		System.out.println("Enter the name/key of the object you would like to delete");
		String objectName = s.nextLine();
		s3.deleteObject(bucketName, objectName);
		
		System.out.println("Deleted" + objectName + " in bucket " + bucketName);
		
	}
	
	
	
	/**
	 * Choose a bucket
	 * generate an html file by completing prompts
	 * @throws IOException
	 */
	private static void createAndAddObjectToBucket() throws IOException {
		
		//	UI
		System.out.println("Delete an object in a bucket from the list below\n");
		listBuckets();
		System.out.println("Enter the name of the bucket you would like to create the object in");
		Scanner s = new Scanner(System.in);
		String bucketName = s.nextLine();
		
		//	gather the data
		System.out.println("Contact First Name: ");
    	String firstName = s.nextLine();
    	
    	System.out.println("Contact Last Name: ");
    	String lastName = s.nextLine();
    	
    	System.out.println("Contact Phone Number:");
    	String number = s.nextLine();
		
    	
    	//	create the html file
    	String fileName = firstName + lastName;
    	File f = File.createTempFile(fileName.toLowerCase(), "html");
    	BufferedWriter bw = new BufferedWriter(new FileWriter(f));
    	bw.write("<html><head><title>Contact Page</title></head><body><table><tr><th>First Name</th><th>Last Name</th><th>Phone Number</th></tr>" +
    			"<tr><td>"+firstName+"</td><td>"+lastName+"</td><td>"+number+"</td></tr></body></html>");
    	bw.close();
    	
    	
    	//lowercase to comply with DNS
		String objectId = fileName.toLowerCase() + ".html";
		s3.putObject(new PutObjectRequest(bucketName, objectId, f));
		
		
	}
	
	
	/**
	 * Edit an object by deleting it and creating a new object
	 * @throws IOException
	 */
	private static void editExistingObject() throws IOException {
		
		System.out.println("Editing an existing object requires you to delete the old object and create a new object");
		deleteObject();
		createAndAddObjectToBucket();
		
	}
	
	
	
	
	
	//===========================================================================
	//	=>	PRIVATE HELPERS
	//===========================================================================
	private static void init() throws Exception {
    	/*
		 * This credentials provider implementation loads your AWS credentials
		 * from a properties file at the root of your classpath.
		 */
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();

        ec2 = new AmazonEC2Client(credentialsProvider);
        s3  = new AmazonS3Client(credentialsProvider);
        sdb = new AmazonSimpleDBClient(credentialsProvider);
    }
	
	
	/**
	 * write the users credentials to a local file
	 * @throws IOException
	 */
	 private static void setKeys() throws IOException{
	    	
	        //	get user input
	    	Scanner s = new Scanner(System.in); 
	    	System.out.println("Access Key: ");
			String secret = s.nextLine();
			System.out.println("Secret Key: ");
			String access = s.nextLine();

			//	prep the file
			File file = new File("src/AwsCredentials.properties");
			FileWriter writer = new FileWriter(file);
			BufferedWriter buffer = new BufferedWriter(writer);
			
			//	write to file
			buffer.write("secretKey=" + secret);
			buffer.newLine();
			buffer.write("accessKey=" + access);
			buffer.close();
	    }
}
